#!/bin/bash

alpha()
{
    echo "abcdefghijklmnopqrstuvwxyz" | \
	sed -E 's/([a-z])/\1 /g'
}

echo "#### From 1"
echo '```'; (seq 1 26 |tr '\n' ' '; echo; alpha) | column -t; echo '```'
echo "#### The decent way (for LED Encryption)"
echo '```'; (seq 0 25 |tr '\n' ' '; echo; alpha) | column -t; echo '```'
