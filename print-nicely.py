#!/usr/bin/env python3
import sys

modules=dict()

def read_input():
    path='categorization.tsv'
    if len(sys.argv)>1:
        path=sys.argv[1]
    with open(path, 'r') as file:
        file.readline()
        for line in file:
            spline=line.split('\t')
            modules[spline[0]] = (int(spline[1]), spline[2], spline[3].strip())

def print_by_levels():
    levels=sorted(set(modules[x][0] for x in modules))
    print("## Modules by defuser engagement level")
    for l in levels:
        print("### Level "+str(l))
        i=1
        for m in modules:
            if modules[m][0]==l:
                print(str(i)+'. **'+m+'**')
                if modules[m][1]!="":
                    print("- Information for the expert:")
                    print('*'+modules[m][1]+'*')
                if modules[m][2]!="":
                    print("- Additional remarks")
                    print('*'+modules[m][2]+'*')
                i=i+1


read_input()
print_by_levels()

    
